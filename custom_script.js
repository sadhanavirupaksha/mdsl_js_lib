function getElementByXpath(src_xpath){
	var ele = document.evaluate(src_xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
	return ele;
}

function replaceNode(new_html, src_xpath){
	// var elem = getElementByXpath(src_xpath); 
	// elem.outerHTML = new_html;
	$(src_xpath).html(new_html);
}

function addNode_link(text, url, add_loc, src_xpath){
	var linkHTML = '<a href="'+url+'">'+text+'</a>';
	addNode(linkHTML, add_loc, src_xpath);
}

function addNode_yt(vdo_id, add_loc, src_xpath){
	embedHTML = '<iframe width="560" height="315" src="//www.youtube.com/embed/'+vdo_id+'" frameborder="0" allowfullscreen></iframe>';
	addNode(embedHTML, add_loc, src_xpath);
}

function addNode_note(note_text, add_loc, src_xpath){
	var cont_style ='width: 960px;margin: 100px auto;';
	var note_style = 'float:left;margin: auto;width: 300px;background: yellow;background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#EBEB00), to(#C5C500));background: -moz-linear-gradient(100% 100% 90deg, #C5C500, #EBEB00);padding: 20px 20px 20px 20px;-webkit-box-shadow: 0px 10px 30px #000;-moz-box-shadow: 0px 10px 30px #000';
	var noteHTML = '<div id="note_container" style="'+cont_style+'"><div class="stick_note" style="'+note_style+'"><p>'+note_text+'</div></div></div>';
	addNode(noteHTML, add_loc, src_xpath);
}

function addNode(innerHTML, add_loc, src_xpath){
	var elem = getElementByXpath(src_xpath); 
	if(add_loc === 'below') elem.insertAdjacentHTML('afterend', innerHTML);
	else elem.insertAdjacentHTML('beforebegin', innerHTML);
}

function addTooltip(new_tt, src_xpath){
	var span = document.createElement('span');
	span.title = new_tt;
	span.id = 'tippy_tt';
	var elem = getElementByXpath(src_xpath);
	wrapInner(elem, span);
}

function wrapInner(parent, wrapper) {
    if (typeof wrapper === "string")
        wrapper = document.createElement(wrapper);

    var div = parent.appendChild(wrapper);

    while(parent.firstChild !== wrapper)
        wrapper.appendChild(parent.firstChild);
}

function convertCurrency(orig, target, conv_rate, str){
// function for currency conversion between different forex.
// possible conversions (orig to target)
// 'inr' to 'usd'
// 'usd' to 'inr'

	var inr_regex = /(Rs|rs|₹)(\.| )*[0-9]+(\/\-)*(INR)*/g;
	var usd_regex = /(\$)(\.| )*[0-9]+(\.)*[0-9]*(\/\-)*(USD)*/g;
	
	var regex2 = /[0-9]+/;	

	var dict = {};
	/*
	{
		value1 : {
			"usd": {
				"value": ,
				"str": 
			}
			"inr": {
				"value": ,
				"str": 
			}
			"org_string": 
		}
	
		value2 : {
			"usd": {
				"value": ,
				"str": 
			}
			"inr": {
				"value": ,
				"str": 
			}
			"org_string": 
		}
	}
	*/

	function fillDict(inp){
		//console.log('in fillDict');
		var value = parseFloat(regex2.exec(inp)[0]);
		dict[value] = {};
		dict[value]['orig_string'] = inp;
		
		if(orig == 'inr'){
			dict[value]['inr'] = {};
			dict[value]['inr']['value'] = value;
			dict[value]['inr']['str'] = 'INR ' + dict[value]['inr']['value'];
			dict[value]['usd'] = {};
			dict[value]['usd']['value'] = value/conv_rate;
			dict[value]['usd']['str'] = 'USD ' + dict[value]['usd']['value'];
		}
		
		if(orig == 'usd'){
			dict[value]['usd'] = {};
			dict[value]['usd']['value'] = value;
			dict[value]['usd']['str'] = 'USD ' + dict[value]['usd']['value'];
			dict[value]['inr'] = {};
			dict[value]['inr']['value'] = value*conv_rate;
			dict[value]['inr']['str'] = 'INR ' + dict[value]['inr']['value'];

		}
		
	}


	// get list of all possible match
	var a = 'a';
	while(a != null) {
		//console.log('in whileloop');
		if(orig == 'inr') a = inr_regex.exec(str);
		else if(orig == 'usd') a = usd_regex.exec(str);
		if(a) fillDict(a[0]);
		//console.log(dict + a);
	}

	for(var i=0; i<Object.keys(dict).length; i++){
		var dict_obj = dict[Object.keys(dict)[i]];
		str = str.replace(dict_obj['orig_string'], dict_obj['orig_string'] + ' (' + dict_obj[target]['str'] + ')');
		//console.log(str);
	}
	return str;
}
//var str = "Freq Used: $5, $10.23, ₹20.00, ₹50.5, ₹100, ₹500 INR, ₹2000";
//console.log(convertCurrency('usd', 'inr', str))

function inject(){
    var tippy_js = document.createElement('script');
    tippy_js.src = 'https://unpkg.com/tippy.js@1.2.0/dist/tippy.js';
    document.getElementsByTagName('body')[0].appendChild(tippy_js);

    var tippy_css = document.createElement('link');
    tippy_css.rel = 'stylesheet';
    tippy_css.type = 'text/css';
    tippy_css.href = 'https://unpkg.com/tippy.js@1.2.0/dist/tippy.css';
    document.getElementsByTagName('head')[0].appendChild(tippy_css);
}

function deleteNode(src_xpath){
	// var elem = getElementByXpath(src_xpath);
	// elem.remove();
	$(src_xpath).remove();
}
// inject();